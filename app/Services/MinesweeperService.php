<?php

namespace App\Services;

use App\Models\BoardConfig;
use App\Models\Game;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class MinesweeperService
{

    public function start(Request $request)
    {

        // get or create board config (minesweeper parameters)
        $boardConfig = BoardConfig::firstOrCreate([
                    'rows'    => $request->get('rows'),
                    'columns' => $request->get('columns'),
                    'mines'   => $request->get('mines'),
        ]);

        // generate blank board
        $row = array_fill(0, $request->get('columns'), Game::STATUS_BLANK);
        $board = array_fill(0, $request->get('rows'), $row);

        // create the game with unique id for the player
        $game = Game::create([
                    'user_id'         => auth()->user()->id,
                    'board_config_id' => $boardConfig->id,
                    'board'           => json_encode($board),
                    'status'          => Game::GAME_STATUS_STARTED,
                    'elapsed_time'    => 0,
        ]);

        return [
            'game_id'      => $game->id,
            'board'        => $this->prepareFinalBoard($board, Game::GAME_STATUS_STARTED),
            'elapsed_time' => 0
        ];
    }

    public function interact(Request $request)
    {
        $row = (int) $request->get('row');
        $column = (int) $request->get('column');

        // retrieve game by its id
        $game = Game::find($request->get('game_id'));

        $board = json_decode($game->board);

        if ($game->status === Game::GAME_STATUS_LOSS) {
            return [
                'board'       => $this->prepareFinalBoard($board, $game->status),
                'game_status' => $game->status
            ];
        }

        // add mines in the board for the first click (mines_set still false),
        // but not in the cell clicked since this is a rule of the game
        if ($game->status === Game::GAME_STATUS_STARTED) {
            $boardConfig = BoardConfig::find($game->board_config_id);

            $board = $this->spreadMinesIntheBoard($board, $boardConfig, $row, $column);

            $game->status = Game::GAME_STATUS_MINES_SET;
        } else {
            $boardConfig = BoardConfig::find($game->board_config_id);
        }

        $cellCurrentStatus = $board[$row][$column];

        // define the new cell status based on the mouse click (left or right)
        $newCellStatus = $this->applyCellRule((int) $cellCurrentStatus, (int) $request->get('action'));

        // check if user clicked in the mine
        if ($newCellStatus === -1) {
            // mark current mine cell with red background
            $board[$row][$column] = -2;
        } else {
            // apply new status
            $board[$row][$column] = $newCellStatus;

            // uncover all adjacents with no mines adjacently (recursively)
            if ($newCellStatus === Game::STATUS_UNCOVERED) {
                $board = $this->uncoverAdjacentCells($board, $row, $column);
            }
        }

        // prepare the board into json type to be persisted and returned to frontend
        $game->board = json_encode($board);

        // define new status with the action performed
        $game->status = $this->defineNewStatus($board, $boardConfig, $newCellStatus);
        $game->elapsed_time = (int) $request->get('elapsed_time');

        $game->save();

        return [
            'board'        => $this->prepareFinalBoard($board, $game->status, $boardConfig),
            'game_status'  => $game->status,
            'elapsed_time' => $game->elapsed_time
        ];
    }

    private function defineNewStatus($board, BoardConfig $boardConfig, $newCellStatus)
    {
        $gameStatus = null;

        // GAME LOSS
        if ($newCellStatus === Game::STATUS_MINE) {
            $gameStatus = Game::GAME_STATUS_LOSS;
        } else {
            // GAME WINNER
            if ($this->isWinner($board, $boardConfig)) {
                $gameStatus = Game::GAME_STATUS_WINNER;
            }
            // GAME CONTINUES
            else {
                $gameStatus = Game::GAME_STATUS_MINES_SET;
            }
        }

        return $gameStatus;
    }

    private function isWinner($board, BoardConfig $boardConfig)
    {

        $victory = false;

        $amountCellsUncoveredToWin = $boardConfig->rows * $boardConfig->columns - $boardConfig->mines;

        $counts = array_count_values(Arr::collapse($board));

        if ($amountCellsUncoveredToWin === $counts[Game::STATUS_UNCOVERED]) {
            $victory = true;
        }


        return $victory;
    }

    private function uncoverAdjacentCells($board, $rowIndex, $columnIndex)
    {

        $adjacentMines = $this->calcAdjacentMines($board, $rowIndex, $columnIndex);

        $adjacentIndexes = [
            -1 => [-1, 0, 1],
            0  => [-1, 1],
            1  => [-1, 0, 1]
        ];

        $boardRows = count($board);
        $boardColumns = count($board[0]);

        $cellsToVisit = [];

        if ($adjacentMines === 0) {
            foreach ($adjacentIndexes as $rowIndexToCheck => $rowToCheck) {
                foreach ($rowToCheck as $columnIndexToCheck) {

                    $finalRowToCheck = $rowIndex + $rowIndexToCheck;
                    $finalColumnToCheck = $columnIndex + $columnIndexToCheck;

                    // uncover adjacent cells if rows / columns limits exist
                    if (
                            $finalRowToCheck >= 0 && $finalRowToCheck < $boardRows &&
                            $finalColumnToCheck >= 0 && $finalColumnToCheck < $boardColumns
                    ) {

                        if ($board[$finalRowToCheck][$finalColumnToCheck] === Game::STATUS_BLANK) {
                            $cellsToVisit[$finalRowToCheck][$finalColumnToCheck] = true;
                            $board[$finalRowToCheck][$finalColumnToCheck] = Game::STATUS_UNCOVERED;
                        }
                    }
                }
            }

            foreach ($cellsToVisit as $rowIndexToCheck => $rowToCheck) {
                foreach ($rowToCheck as $columnIndexToCheck => $value) {
                    $board = $this->uncoverAdjacentCells($board, $rowIndexToCheck, $columnIndexToCheck);
                }
            }
        } else {
            if ($board[$rowIndex][$columnIndex] === Game::STATUS_BLANK) {
                $board[$rowIndex][$columnIndex] = Game::STATUS_UNCOVERED;
            }
        }

        return $board;
    }

    /**
     * This function does the following: 
     *    - hides the mines to show to user as blank cells
     *    - show the number of adjacent mines
     * 
     * @param array $board
     * @param string $gameStatus
     * @param BoardConfig $boardConfig
     * @return array
     */
    private function prepareFinalBoard($board, $gameStatus, BoardConfig $boardConfig = null)
    {

        $finalBoard = [];
        foreach ($board as $rowIndex => $row) {
            foreach ($row as $columnIndex => $value) {
                $finalBoard[$rowIndex][$columnIndex] = $this->prepareFinalCell($board, $gameStatus, $rowIndex, $columnIndex, $boardConfig);
            }
        }

        return $finalBoard;
    }

    private function prepareFinalCell($board, $gameStatus, $rowIndex, $columnIndex, ?BoardConfig $boardConfig)
    {

        $finallCell = [];

        $value = $board[$rowIndex][$columnIndex];

        // GAME CONTINUES
        if ($gameStatus !== Game::GAME_STATUS_LOSS) {
            if ($value === Game::STATUS_MINE_FLAGGED) {
                $finallCell['status'] = Game::STATUS_FLAGGED;
            } elseif ($value === Game::STATUS_MINE_QUESTION_MARK) {
                $finallCell['status'] = Game::STATUS_QUESTION_MARK;
            } elseif ($value === Game::STATUS_MINE) {
                $finallCell['status'] = Game::STATUS_BLANK;
            } elseif ($value === Game::STATUS_BLANK) {
                $finallCell['status'] = Game::STATUS_BLANK;
            } elseif ($value === Game::STATUS_FLAGGED) {
                $finallCell['status'] = Game::STATUS_FLAGGED;
            } elseif ($value === Game::STATUS_QUESTION_MARK) {
                $finallCell['status'] = Game::STATUS_QUESTION_MARK;
            } else {
                $finallCell['status'] = Game::STATUS_UNCOVERED;
                $adjacentMines = $this->calcAdjacentMines($board, $rowIndex, $columnIndex);
                $finallCell['value'] = $adjacentMines > 0 ? $adjacentMines : '';
            }
        }
        // GAME LOSS
        else {
            if ($value < 0) {
                $finallCell['status'] = ($value === -2) ? Game::STATUS_MINE_CLICKED : Game::STATUS_MINE;
            } else {
                $finallCell['status'] = Game::STATUS_UNCOVERED;
            }
            $adjacentMines = $this->calcAdjacentMines($board, $rowIndex, $columnIndex);
            $finallCell['value'] = $adjacentMines > 0 ? $adjacentMines : '';
        }

        return $finallCell;
    }

    private function calcAdjacentMines($board, $rowIndex, $columnIndex)
    {
        $numberOfAdjacentMines = 0;

        // check mines in row above
        // check adjacent above left column
        $numberOfAdjacentMines = $this->isMineCell($board, $rowIndex - 1, $columnIndex - 1) ? ++$numberOfAdjacentMines : $numberOfAdjacentMines;
        // check adjacent above center column
        $numberOfAdjacentMines = $this->isMineCell($board, $rowIndex - 1, $columnIndex) ? ++$numberOfAdjacentMines : $numberOfAdjacentMines;
        // check adjacent above right column
        $numberOfAdjacentMines = $this->isMineCell($board, $rowIndex - 1, $columnIndex + 1) ? ++$numberOfAdjacentMines : $numberOfAdjacentMines;

        // check mines in row below
        // check adjacent below left column
        $numberOfAdjacentMines = $this->isMineCell($board, $rowIndex + 1, $columnIndex - 1) ? ++$numberOfAdjacentMines : $numberOfAdjacentMines;
        // check adjacent below center column
        $numberOfAdjacentMines = $this->isMineCell($board, $rowIndex + 1, $columnIndex) ? ++$numberOfAdjacentMines : $numberOfAdjacentMines;
        // check adjacent below right column            
        $numberOfAdjacentMines = $this->isMineCell($board, $rowIndex + 1, $columnIndex + 1) ? ++$numberOfAdjacentMines : $numberOfAdjacentMines;

        // check mines in current row
        // check adjacent left column
        $numberOfAdjacentMines = $this->isMineCell($board, $rowIndex, $columnIndex - 1) ? ++$numberOfAdjacentMines : $numberOfAdjacentMines;
        // check adjacent right column
        $numberOfAdjacentMines = $this->isMineCell($board, $rowIndex, $columnIndex + 1) ? ++$numberOfAdjacentMines : $numberOfAdjacentMines;

        return $numberOfAdjacentMines;
    }

    private function isMineCell($board, $rowIndex, $columnIndex)
    {
        $isMineCell = false;

        if (isset($board[$rowIndex][$columnIndex]) && $board[$rowIndex][$columnIndex] < 0) {
            $isMineCell = true;
        }

        return $isMineCell;
    }

    private function spreadMinesIntheBoard($board, BoardConfig $boardConfig, $rowClicked, $columnClicked)
    {
        // flatten the multi array to one dimension array
        // this is a preparation to generate the mines randomly
        $boardFlattened = Arr::collapse($board);

        // generating the positions of the mines
        $randNumbersArray = range(0, count($boardFlattened) - 1);
        shuffle($randNumbersArray);
        $minesIndexes = array_slice($randNumbersArray, 0, $boardConfig->mines);

        // setting the mines into the board
        foreach ($minesIndexes as $mineIndex) {
            $boardFlattened[$mineIndex] = -1;
        }

        // return the flattened array to multi array (board game format)
        // that is, separated the flattened array into chunks which the "size = number of columns"
        $board = array_chunk($boardFlattened, $boardConfig->columns);

        // if by chance there is a mine in the same position of the first click cell
        // then replace it manually
        $board = $this->changeFirstClickedCellIfMine($board, $rowClicked, $columnClicked);

        return $board;
    }

    private function changeFirstClickedCellIfMine($board, $rowClicked, $columnClicked)
    {
        // change the mine position if by chance it was clicked first
        if ($board[$rowClicked][$columnClicked] === -1) {

            foreach ($board as $rowIndex => $row) {
                foreach ($row as $columnIndex => $value) {
                    if ($value === Game::STATUS_BLANK) {
                        // assign the mine to be replaced to the first blank cell available
                        $board[$rowIndex][$columnIndex] = Game::STATUS_MINE;
                        break 2;
                    }
                }
            }

            // assign the first cell (bomb) to a blank one!
            $board[$rowClicked][$columnClicked] = Game::STATUS_BLANK;
        }

        return $board;
    }

    private function applyCellRule($cellCurrentStatus, $action)
    {
        $newCellStatus = $cellCurrentStatus;

        // considering the current status of the cell
        // let's determine the new status according to the action performed
        switch ($action) {
            case Game::CLICK_LEFT:
                $newCellStatus = $this->applyLeftClickRule($cellCurrentStatus);
                break;
            case Game::CLICK_RIGHT:
                $newCellStatus = $this->applyRightClickRule($cellCurrentStatus);
                break;
            default:
                break;
        }

        return $newCellStatus;
    }

    private function applyLeftClickRule($cellCurrentStatus)
    {
        return ($cellCurrentStatus < 0) ? Game::STATUS_MINE : Game::STATUS_UNCOVERED;
    }

    private function applyRightClickRule($cellCurrentStatus)
    {
        $newCellStatus = null;

        // BLANK
        if ($cellCurrentStatus === Game::STATUS_BLANK) {
            $newCellStatus = Game::STATUS_FLAGGED;
        }
        // FLAGGED
        elseif ($cellCurrentStatus === Game::STATUS_FLAGGED) {
            $newCellStatus = Game::STATUS_QUESTION_MARK;
        }
        // QUESTION
        elseif ($cellCurrentStatus === Game::STATUS_QUESTION_MARK) {
            $newCellStatus = Game::STATUS_BLANK;
        }
        // MINE
        elseif ($cellCurrentStatus === Game::STATUS_MINE) {
            $newCellStatus = Game::STATUS_MINE_FLAGGED;
        }
        // MINE FLAGGED
        elseif ($cellCurrentStatus === Game::STATUS_MINE_FLAGGED) {
            $newCellStatus = Game::STATUS_MINE_QUESTION_MARK;
        }
        // MINE QUESTION MARK
        elseif ($cellCurrentStatus === Game::STATUS_MINE_QUESTION_MARK) {
            $newCellStatus = Game::STATUS_MINE_HIDE;
        }
        // MINE HIDE (after a full cycle of right clicking => hidden again)
        elseif ($cellCurrentStatus === Game::STATUS_MINE_HIDE) {
            $newCellStatus = Game::STATUS_MINE_FLAGGED;
        }

        return $newCellStatus;
    }

    public function resumeGame(Request $request)
    {
        // get or create board config (minesweeper parameters)
        $game = Game::find((int) $request->get('game_id'));

        return [
            'game_id'      => $game->id,
            'board'        => $this->prepareFinalBoard(json_decode($game->board), $game->status),
            'elapsed_time' => $game->elapsed_time
        ];
    }

}
