<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Services\MinesweeperService;
use Illuminate\Http\Request;

class MinesweeperController extends Controller
{

    public function start(Request $request)
    {
        return (new MinesweeperService)->start($request);
    }

    public function interact(Request $request)
    {
        return (new MinesweeperService)->interact($request);
    }

    public function scores(Request $request)
    {
        return (new Game)->getScores();
    }

    public function getResumeGames(Request $request)
    {
        return (new Game)->getResumeGames();
    }
    
    public function resumeGame(Request $request)
    {
        return (new MinesweeperService)->resumeGame($request);
    }

}
