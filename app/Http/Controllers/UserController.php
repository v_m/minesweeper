<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Sanctum\PersonalAccessToken;
use function auth;
use function view;

class UserController extends Controller
{

    public function index()
    {
        return view('user.index');
    }

    public function register(Request $request)
    {

        $rules = [
            'register_name'     => 'required',
            'register_email'    => 'required',
            'register_password' => 'required',
        ];

        if (Validator::make($request->all(), $rules)->passes()) {

            $user = User::create([
                        'name'     => $request->get('register_name'),
                        'email'    => $request->get('register_email'),
                        'password' => Hash::make($request->get('register_password')),
            ]);

            if ($user) {
                Auth::login($user);

                return [
                    'message' => 'You are registered and logged in!<br />',
                    'params'  => [
                        'token' => auth()->user()->createToken(auth()->user()->email)->plainTextToken,
                        'email' => auth()->user()->email,
                        'name' => auth()->user()->name
                    ],
                    'success' => true
                ];
            }
        } else {
            return ['success' => false, 'message' => 'Register failed!'];
        }
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->login_email, 'password' => $request->login_password])) {

            return [
                'message' => 'You are registered and logged in!<br />',
                'params'  => [
                    'token' => auth()->user()->createToken(auth()->user()->email)->plainTextToken,
                    'email' => auth()->user()->email,
                    'name' => auth()->user()->name
                ],
                'success' => true
            ];
        } else {
            return ['success' => false, 'message' => 'login failed!'];
        }
    }

    public function logout()
    {
        PersonalAccessToken::findToken(str_ireplace('Bearer ', '', request()->header('Authorization')))->delete();

        Auth::guard('web')->logout();
        
        return ['message' => 'You are now logged out!'];
    }

}
