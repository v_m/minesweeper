<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Game extends Model
{

    public const STATUS_MINE_HIDE = -5;
    public const STATUS_MINE_QUESTION_MARK = -4;
    public const STATUS_MINE_FLAGGED = -3;
    public const STATUS_MINE_CLICKED = -2;
    public const STATUS_MINE = -1;
    public const STATUS_BLANK = 0;
    public const STATUS_UNCOVERED = 1;
    public const STATUS_FLAGGED = 2;
    public const STATUS_QUESTION_MARK = 3;
    public const CLICK_LEFT = 1;
    public const CLICK_RIGHT = 3;
    public const GAME_STATUS_STARTED = 'STARTED';
    public const GAME_STATUS_MINES_SET = 'MINES_SET';
    public const GAME_STATUS_WINNER = 'WINNER';
    public const GAME_STATUS_LOSS = 'LOSS';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'board_config_id',
        'board',
        'status',
        'elapsed_time',
    ];

    public static function getScores()
    {
        $scoresRaw = DB::table('games')
                ->select('users.name', 'games.elapsed_time', 'board_configs.rows', 'board_configs.columns', 'board_configs.mines')
                ->leftJoin('board_configs', 'games.board_config_id', '=', 'board_configs.id')
                ->leftJoin('users', 'users.id', '=', 'games.user_id')
                ->where('games.status', self::GAME_STATUS_WINNER)
                ->orderBy('board_configs.rows', 'desc')
                ->orderBy('board_configs.columns', 'desc')
                ->orderBy('board_configs.mines', 'desc')
                ->orderBy('games.elapsed_time', 'asc')
                ->get();

        $scores = [];
        foreach ($scoresRaw as $scoreRaw) {
            $gameBoardType = $scoreRaw->rows . 'x' . $scoreRaw->columns . '|' . $scoreRaw->mines;
            $scores[$gameBoardType][] = [
                'user'         => $scoreRaw->name,
                'elapsed_time' => $scoreRaw->elapsed_time . ' seconds'
            ];
        }

        return $scores;
    }

    public static function getResumeGames()
    {
        $scoresRaw = DB::table('games')
                ->select('games.id', 'users.name', 'games.elapsed_time', 'board_configs.rows', 'board_configs.columns', 'board_configs.mines')
                ->leftJoin('board_configs', 'games.board_config_id', '=', 'board_configs.id')
                ->leftJoin('users', 'users.id', '=', 'games.user_id')
                ->where('games.user_id', auth()->user()->id)
                ->whereIn('games.status', [self::GAME_STATUS_STARTED, self::GAME_STATUS_MINES_SET])
                ->orderBy('games.id', 'desc')
                ->get();

        $scores = [];
        foreach ($scoresRaw as $scoreRaw) {
            $gameBoardType = $scoreRaw->rows . 'x' . $scoreRaw->columns . '|' . $scoreRaw->mines;
            $scores[] = [
                'game_id'      => $scoreRaw->id,
                'board_type'   => $gameBoardType,
                'elapsed_time' => $scoreRaw->elapsed_time . ' seconds'
            ];
        }

        return $scores;
    }

}
