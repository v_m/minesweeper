<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Minesweeper</title>

        <!-- Styles -->
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" href="{{ asset('css/minesweeper.css') }}">

        <!-- Scripts -->
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="{{ asset('js/minesweeper.js') }}" defer></script>
    </head>
    <body class="antialiased">

        <div id="minesweeper_window">

            <div class="top_menu">
                <div class="buttons">
                    <div class="button close"></div>
                    <div class="button minimize"></div>
                    <div class="button maximize"></div>
                </div>
                <div class="title">Minesweeper</div>
                <span id="userinfo">
                    <span id="username"></span>
                    <span id="timer" class="label label-primary" style="display: none;">
                        <span id="timer-value">0</span>                
                        <span id="timer-unit"></span>
                    </span>        
                    <span id="scores-button-area">
                        <!-- Button trigger modal -->
                        <button type="button" id="scores-button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#scores">
                            Scores
                        </button>
                    </span>
                    <span id="resume-games"  style="display: none;">
                        <span id="resume-button" class="btn btn-light btn-sm"  data-toggle="modal" data-target="#resume-games-modal">
                            Resume<br />Games
                        </span>
                    </span>
                    <span id="restart-game"  style="display: none;">
                        <span id="restart-button" class="btn btn-light btn-sm">
                            Restart
                        </span>
                    </span>
                </span>                
            </div>

            <div id="main-screen">

                <div id="user" class="col-sm-12">
                    <div id="register"  class="text-center col-sm-4 ">
                        <form id="register-form"  class="col-sm-10">
                            <div class="form-group row">
                                <label for="register_name">Name</label>
                                <input type="text" class="form-control" id="register_name" name="register_name" aria-describedby="register_name">
                            </div>
                            <div class="form-group row">
                                <label for="register_email">Email</label>
                                <input type="text" class="form-control" id="register_email" name="register_email" aria-describedby="register_email">
                            </div>
                            <div class="form-group row">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="register_password" name="register_password" aria-describedby="register_password" >
                            </div>
                            <button type="submit" class="btn btn-primary" id="register">Register</button>
                        </form>

                        <span id="register-success-alert" style="display:none"/>

                    </div>
                    <div id="login"  class="text-center col-sm-4 col-sm-offset-3">
                        <form id="login-form"  class="col-sm-10">
                            <div class="form-group row">
                                <label for="login_email">Email</label>
                                <input type="text" class="form-control" id="login_email" name="login_email" aria-describedby="login_email" >
                            </div>
                            <div class="form-group row">
                                <label for="login_password">Password</label>
                                <input type="password" class="form-control" id="login_password" name="login_password" aria-describedby="login_password">
                            </div>
                            <button type="submit" class="btn btn-primary" id="login">Login</button>
                        </form>

                        <span id="login-success-alert" style="display:none" /> 
                    </div>
                </div>
            </div>
        </div>


        <!-- Scores Modal -->
        <div class="modal fade" id="scores" tabindex="-1" role="dialog" aria-labelledby="scores">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Scores</h4>
                    </div>
                    <div class="modal-body">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Resume Games Modal -->
        <div class="modal fade" id="resume-games-modal" tabindex="-1" role="dialog" aria-labelledby="resume-games-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Resume Games</h4>
                    </div>
                    <div class="resume-games-modal-body">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
        <span id="game-id-config"  style="display: none;">
            
        </span>
    </body>
</html>
