<?php

use App\Http\Controllers\MinesweeperController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// GUEST
Route::post('/register', [UserController::class, 'register']);
Route::post('/login', [UserController::class, 'login']);
Route::get('/scores', [MinesweeperController::class, 'scores']);

// USER (logged in)
Route::middleware('auth:sanctum')->post('/start', [MinesweeperController::class, 'start']);
Route::middleware('auth:sanctum')->post('/interact', [MinesweeperController::class, 'interact']);
Route::middleware('auth:sanctum')->post('/interact', [MinesweeperController::class, 'interact']);
Route::middleware('auth:sanctum')->get('/resume-games', [MinesweeperController::class, 'getResumeGames']);
Route::middleware('auth:sanctum')->post('/resume-game', [MinesweeperController::class, 'resumeGame']);
Route::post('/auth/logout', [UserController::class, 'logout']);
