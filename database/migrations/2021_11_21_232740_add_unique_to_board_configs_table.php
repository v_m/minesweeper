<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUniqueToBoardConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('board_configs', function (Blueprint $table) {
            $table->unique(['rows', 'columns', 'mines'], 'board_config_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('board_configs', function (Blueprint $table) {
            $table->dropUnique('board_config_unique');
        });
    }
}
