$(document).ready(function () {
    var refreshIntervalId;
    var token;
    var gameConfig;

    // disable mouse right click
    $(document)[0].oncontextmenu = function () {
        return false;
    }

    // scores
    $('#scores-button').click(function () {
        // send request to API to generate a new minesweeper board
        $.ajax({
            url: 'api/scores',
            type: 'get',
            headers: {
                'Accept': 'application/json'
            },
            dataType: 'json',
            success: function (data) {
                renderScores(data);
            }
        });
    });

    function renderScores(data) {
        for (const [key, value] of Object.entries(data)) {
            $('.modal-body').append("<h4>" + key + "</h4>");
            value.forEach(function (item) {
                $('.modal-body').append(item.user + " in " + item.elapsed_time + "<br />");
            });
            $('.modal-body').append("<br />");
        }
    }

    // resume game
    $('#resume-button').click(function () {
        // send request to API to generate a new minesweeper board
        $.ajax({
            url: 'api/resume-games',
            type: 'get',
            headers: {
                'Authorization': 'Bearer ' + token,
                'Accept': 'application/json'
            },
            dataType: 'json',
            success: function (data) {
                renderGamesToResume(data);
            }
        });
    });

    function renderGamesToResume(data) {
        data.forEach(function (item) {
            $('.resume-games-modal-body').append("#<span class=\"game_id_to_resume\">" + item.game_id + "</span> (" + item.board_type + ") stopped in " + item.elapsed_time + "<br />");
        });
    }
    
    $(document).on("click", ".game_id_to_resume", function () {
        $('#resume-games-modal').modal('hide');
        resumeGame($(this).text());
    });

    function resumeGame(gameId) {
        event.preventDefault();

        // clear board area
        $('#minesweeper').empty();

        // send request to API to generate a new minesweeper board
        $.ajax({
            url: 'api/resume-game',
            type: 'post',
            data: {'game_id': gameId},
            headers: {
                'Authorization': 'Bearer ' + token,
                'Accept': 'application/json'
            },
            dataType: 'json',
            success: function (data) {
                $('#game-id-config').html("<div id=\"game-id\" data=\"" + data.game_id + "\" />");

                // render board game
                buildBoard(data.board);

                // set elapsed time
                setTimer(data.elapsed_time);
                $('#restart-game').show();
            }
        });
    }

    // register
    $('#register-form').submit(function () {

        event.preventDefault();

        // send request to API to generate a new minesweeper board
        $.ajax({
            url: 'api/register',
            type: 'post',
            data: $(this).serializeArray(),
            headers: {
                'Accept': 'application/json'
            },
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    token = data.params.token;
                    $('#resume-games').show();
                    renderGameOptions(data);
                } else {
                    $("#register-success-alert").text('failed').fadeTo(2000, 500).slideUp(1000, function () {
                        $("#register-success-alert").slideUp(1000);
                    });
                }
            }
        });
    });

    // login
    $('#login-form').submit(function () {

        event.preventDefault();

        // send request to API to generate a new minesweeper board
        $.ajax({
            url: 'api/login',
            type: 'post',
            data: $(this).serializeArray(),
            headers: {
                'Accept': 'application/json'
            },
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    token = data.params.token;
                    $('#resume-games').show();
                    renderGameOptions(data);
                } else {
                    $("#login-success-alert").text('failed').fadeTo(2000, 500).slideUp(1000, function () {
                        $("#login-success-alert").slideUp(1000);
                    });
                }
            }
        });
    });


    function checkRules() {
        var rows = parseInt($('#rows').val());
        var columns = parseInt($('#columns').val());
        var mines = parseInt($('#mines').val());
        var areRulesOk = true;

        if (mines >= rows * columns) {
            alert('Number of mines should be less than number of cells (rows * columns)');
            areRulesOk = false;
        }

        if (rows < 4 || columns < 4) {
            alert('Rows and Columns must be at least 4');
            areRulesOk = false;
        }

        if (rows > 16 || columns > 16) {
            alert('Rows and Columns max values are 16');
            areRulesOk = false;
        }

        return areRulesOk;
    }

    function startNewGame() {
        event.preventDefault();

        if (!checkRules())
            return false;

        // clear board area
        $('#minesweeper').empty();

        // send request to API to generate a new minesweeper board
        $.ajax({
            url: 'api/start',
            type: 'post',
            data: gameConfig,
            headers: {
                'Authorization': 'Bearer ' + token,
                'Accept': 'application/json'
            },
            dataType: 'json',
            success: function (data) {
                $('#game-id-config').html("<div id=\"game-id\" data=\"" + data.game_id + "\" />");

                // render board game
                buildBoard(data.board);

                // set elapsed time
                setTimer(0);
                $('#restart-game').show();
            }
        });
    }

    // start game
    $(document).on("submit", "#start-form", function () {
        gameConfig = $(this).serializeArray();
        startNewGame();
    });

    // restart game
    $(document).on("click", "#restart-button", function () {
        startNewGame();
    });

    $(document).on("mousedown", ".cell", function (event) {

        event.preventDefault();

        // send request to API to generate a new minesweeper board
        $.ajax({
            url: 'api/interact',
            type: 'post',
            data: {
                row: $(this).attr('row'),
                column: $(this).attr('column'),
                action: event.which,
                game_id: $('#game-id').attr('data'),
                elapsed_time: $('#timer-value').text()
            },
            headers: {
                'Authorization': 'Bearer ' + token,
                'Accept': 'application/json'
            },
            dataType: 'json',
            success: function (data) {
                // render board game
                buildBoard(data.board);

                if (data.game_status === 'WINNER' || data.game_status === 'LOSS') {
                    if (data.game_status === 'WINNER') {
                        $('#minesweeper').prepend("<p class='winner'>WINNER</p><br />");
                    }
                    // set elapsed time (static)
                    setTimer(data.elapsed_time, true);
                }
            }
        });

        return false;
    });

    function setTimer(elapsed_time, static = false) {
        var timestampNow = Date.now();

        if (!static) {
            if (refreshIntervalId !== undefined) {
                clearInterval(refreshIntervalId);
            }
            refreshIntervalId = setInterval(function () {
                $('#timer-value').text(parseInt(((new Date() - timestampNow) / 1000) + elapsed_time));
            }, 1000);
        } else {
            /* later */
            clearInterval(refreshIntervalId);
            $('#timer-value').text(elapsed_time);
        }

        $('#timer-unit').text("seconds");

        $('#timer').show();
    }

    function buildBoard(board) {
        var value = '';

        // clear board area
        $('#minesweeper').empty();

        for (row = 0; row < board.length; row++) {
            for (column = 0; column < board[row].length; column++) {
                value = board[row][column]['value'] ? board[row][column]['value'] : '&nbsp;';
                $("#minesweeper").append("<button class=\"cell status" + board[row][column]['status'] + "\" row=\"" + row + "\" column=\"" + column + "\" style=\";height:40px;width:40px;\">" + value + "</button>");
            }

            $("#minesweeper").append("<br/>");
        }
    }

    function renderGameOptions(data) {
        $("#main-screen").html('<div id="minesweeper"  class="text-center"><form id="start-form"  class="col-sm-4 col-sm-offset-4">    <div class="form-group row">        <label for="rows">Number of rows</label>        <input type="text" class="form-control" id="rows" value="8" name="rows" aria-describedby="rows" placeholder="8">    </div>    <div class="form-group row">        <label for="columns">Number of columns</label>        <input type="text" class="form-control" id="columns" value="8" name="columns" aria-describedby="columns" placeholder="8">    </div>    <div class="form-group row">        <label for="mines">Number of mines</label>        <input type="text" class="form-control" id="mines" value="10" name="mines" aria-describedby="mines" placeholder="10">    </div>    <button type="submit" class="btn btn-primary" id="start">Start</button></form></div>');
        $('#username').html("User: <b>" + data.params.name + "</b>");
    }
});
